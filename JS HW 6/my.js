function createNewUser() {
    let newUser = {};
    
    newUser.firstName = prompt("Введіть ім'я:");
    newUser.lastName = prompt("Введіть прізвище:");
    newUser.birthday = prompt("Введіть дату народження (у форматі dd.mm.yyyy):");
  
    newUser.getLogin = function() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    };
    
    newUser.getAge = function() {
      let today = new Date();
      let birthDate = new Date(this.birthday);
      let age = today.getFullYear() - birthDate.getFullYear();
      let month = today.getMonth() - birthDate.getMonth();
      if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    };
    
    newUser.getPassword = function() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
    };
    
    return newUser;
  }
  
  let user = createNewUser();
  console.log(user);
  console.log("Age:", user.getAge());
  console.log("Password:", user.getPassword());
  