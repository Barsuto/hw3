function filterBy(array, type) {
    return array.filter(item => typeof item !== type);
  }
  
  let myArray = ['hello', 'world', 23, '23', null];
  let filteredArray = filterBy(myArray, 'string');
  console.log(filteredArray); 