function createNewUser() {
    const newUser = {
      firstName: prompt("Введіть ім'я:"),
      lastName: prompt("Введіть прізвище:"),
      getLogin() {
        return `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`
      },
      setFirstName(newName) {
        this.firstName = newName;
      },
      setLastName(newName) {
        this.lastName = newName;
      }
    };
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());